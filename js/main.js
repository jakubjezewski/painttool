class PaintTool {
  constructor(elementId) {
    if (!elementId || typeof elementId !== 'string') {
      throw new Error(`Nie podano elementu DOM lub nie jest on typu 'string'`)
    }

    // div do którego zagnieżdżony jest rysownik
    this.parentNode = document.getElementById(elementId);

    // główny div
    this.node = document.createElement('div');
    this.node.classList.add('painttool-main');

    // canvas
    this.canvas = document.createElement('canvas');
    this.canvas.classList.add('painttool-canvas');
    this.ctx = this.canvas.getContext('2d');

    // pozycja kursora
    this.currentPosition = {
      x: 0,
      y: 0,
    }

    this.previousPosition = {
      x: 0,
      y: 0,
    }

    // aktualizacja pozycji kursora podczas ruchu myszki
    this.canvas.addEventListener('mousedown', (e) => this.update(e));
    this.canvas.addEventListener('mouseup', (e) => this.update(e));
    this.canvas.addEventListener('mousemove', (e) => this.update(e));
    this.canvas.addEventListener('mouseout', (e) => this.update(e));

    // aktualizacja skali po zmianie rozmiaru okna
    window.addEventListener('resize', () => this.scale());
    this.scale();

    // dodanie elementów do DOM
    this.parentNode.append(this.node);

    this.node.innerHTML += `
      <div class="painttool-header">
        <div class="painttool-header-title">
          <h1>PaintTool</h1>
        </div>
      </div>
    `;
    this.node.append(this.canvas);


    // załadowanie paska
    this.loadBar();
  }

  loadBar() {
    const header = document.querySelector('.painttool-header');

    // rozmiar rysowanych linii
    const sizeElement = document.createElement('div');
    sizeElement.classList.add('painttool-header-size');

    const sizeInput = document.createElement('input');
    sizeInput.setAttribute('type', 'range');
    sizeInput.setAttribute('min', '1');
    sizeInput.setAttribute('max', '15');
    sizeInput.setAttribute('value', '1');

    sizeInput.addEventListener('input', () => {
      this.ctx.lineWidth = sizeInput.value;
    });

    sizeElement.innerHTML += `<span>Rozmiar</span>`;
    sizeElement.append(sizeInput);

    // kolor rysowanej linii
    const colorElement = document.createElement('div');
    colorElement.classList.add('painttool-header-color');

    const colorInput = document.createElement('input');
    colorInput.setAttribute('type', 'color');
    colorInput.setAttribute('value', '#000000');

    colorInput.addEventListener('input', () => {
      this.ctx.strokeStyle = colorInput.value;
    });

    colorElement.innerHTML += `<span>Kolor</span>`;
    colorElement.append(colorInput);

    // wyczyszczenie
    const clearElement = document.createElement('div');
    clearElement.classList.add('painttool-header-erase');

    clearElement.innerHTML += `<span>Wyczyść</span>`;
    clearElement.addEventListener('click', () => {
      this.erase();
    });

    // dodanie elementów do paska
    header.append(sizeElement);
    header.append(colorElement);
    header.append(clearElement);
  }

  scale() {
    this.canvas.width =  window.innerWidth;
    this.canvas.height = window.innerHeight * 0.91;
  }

  update(event) {
    const type = event.type;

    // jeśli zostanie przyciśnięty lewy przycisk myszy
    if (type == 'mousedown') {
      this.previousPosition.x = this.currentPosition.x;
      this.previousPosition.y = this.currentPosition.y;

      this.currentPosition.x = event.clientX - this.canvas.offsetLeft;
      this.currentPosition.y = event.clientY - this.canvas.offsetTop;

      this.drawing = true;
    }

    // jeśli lewy przycisk zostanie puszczony lub kursor wyjdzie za obszar rysowania
    if (type === 'mouseup' || type === 'mouseout') {
      this.drawing = false;
    }

    // jeśli mysz się poruszy
    if (type === 'mousemove') {
      if (this.drawing) {
        this.previousPosition.x = this.currentPosition.x;
        this.previousPosition.y = this.currentPosition.y;

        this.currentPosition.x = event.clientX - this.canvas.offsetLeft;
        this.currentPosition.y = event.clientY - this.canvas.offsetTop;
        this.draw();
      }
    }
  }

  erase() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  draw() {
    this.ctx.beginPath();

    this.ctx.moveTo(this.previousPosition.x, this.previousPosition.y);
    this.ctx.lineTo(this.currentPosition.x, this.currentPosition.y);

    // narysowanie ścieżki
    this.ctx.stroke();
  }
}

window.onload = () => {
  tool = new PaintTool('app');
};